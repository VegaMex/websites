<?php
   $title = "VideoLAN Security Bulletin VLC 3.0.8";
   $lang = "en";
   $menu = array( "vlc" );
   $body_color = "red";
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
?>


<div id="fullwidth">

<h1>Security Bulletin VLC 3.0.8</h1>
<pre>
Summary           : Multiple vulnerabilities fixed in VLC media player
Date              : August 2019
Affected versions : VLC media player 3.0.7.1 and earlier for most issues
ID                : VideoLAN-SB-VLC-308
CVE references    : CVE-2019-13602, CVE-2019-13962, CVE-2019-14437, CVE-2019-14438, CVE-2019-14498, CVE-2019-14533, CVE-2019-14534, CVE-2019-14535, CVE-2019-14776, CVE-2019-14777, CVE-2019-14778, CVE-2019-14970
</pre>

<h2>Details</h2>
<p>A remote user could create a specifically crafted file that could trigger issues ranging from buffer overflows to division by zero.</p>

<h2>Impact</h2>
<p>If successful, a malicious third party could trigger either a crash of VLC or an arbitratry code execution with the privileges of the target user.</p>
<p>While these issues in themselves are most likely to just crash the player, we can't exclude that they could be combined to leak user informations or 
remotely execute code. ASLR and DEP help reduce the likelyness of code execution, but may be bypassed.</p>
<p>We have not seen exploits performing code execution through these vulnerabilities</p>
<br />
<p>While CVE CVE-2019-13602 &amp; CVE-2019-13962 mention a base score of 8.8 and 9.8 respectively, the VideoLAN team believes this severity is highly exagerated; in our 
opinion, a base score of 4.3 (AV:N/AC:L/PR:N/UI:R/S:U/C:N/I:N/A:L) would be more reasonable.</p>
<p>CVE-2019-13962 only affects VLC 3.0.2 to 3.0.7.1

<h2>Threat mitigation</h2>
<p>Exploitation of those issues requires the user to explicitly open a specially crafted file or stream.</p>

<h2>Workarounds</h2>
<p>The user should refrain from opening files from untrusted third parties
or accessing untrusted remote sites (or disable the VLC browser plugins),
until the patch is applied.
</p>

<h2>Solution</h2>
<p>VLC media player <b>3.0.8</b> addresses the issues.
</p>

<h2>Credits</h2>
<p>CVE-2019-14437, CVE-2019-14438, CVE-2019-14498, CVE-2019-14533, CVE-2019-14534, CVE-2019-14535, CVE-2019-14776, CVE-2019-14777, CVE-2019-14778, CVE-2019-14970 were reported by Antonio Morales from the Semmle Security Team</p>
<p>Two issues with pending CVE IDs were reported by Scott Bell from Pulse Security</p>
<p>CVE-2019-13602 was reported by Hyeon-Ju Lee</p>
<p>CVE-2019-13962 was reported by Xinyu Liu</p>

<h2>Special thanks</h2>
<p>VideoLAN would like to thank Antonio Morales and Scott Bell for their time and cooperation with the VideoLAN security team</p>

<h2>References</h2>
<dl>
<dt>The VideoLAN project</dt>
<dd><a href="//www.videolan.org/">http://www.videolan.org/</a>
</dd>
<dt>VLC official GIT repository</dt>
<dd><a href="http://git.videolan.org/?p=vlc/vlc-3.0.git">http://git.videolan.org/?p=vlc.git</a>
</dd>
</dl>

</div>

<?php footer('$Id$'); ?>
