# Luxembourgish translation
# Copyright (C) 2020 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Thierry Yves Gaston Stein, 2014,2016
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-10-06 14:28+0000\n"
"Last-Translator: VideoLAN <videolan@videolan.org>, 2017\n"
"Language-Team: Luxembourgish (http://www.transifex.com/yaron/vlc-trans/"
"language/lb/)\n"
"Language: lb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "e Projet an en"

#: include/header.php:289
msgid "non-profit organization"
msgstr "gemengnëtzech Organisatioun"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Partneren"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Team &amp; Organisatioun"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Berodungsleeschtungen &amp; Partner"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Veranstaltungen"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Rechtleches"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Presseberäich"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Kontaktéiert eis"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Download"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Funktiounen"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Upassen"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Goodien eroflueden"

#: include/menus.php:51
msgid "Projects"
msgstr "Projeten"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "All Projeten"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Matmaachen"

#: include/menus.php:77
msgid "Getting started"
msgstr "Lassleeën"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "En Don maachen"

#: include/menus.php:79
msgid "Report a bug"
msgstr "E Bug mellen"

#: include/menus.php:83
msgid "Support"
msgstr "Support"

#: include/footer.php:33
msgid "Skins"
msgstr "Themen"

#: include/footer.php:34
msgid "Extensions"
msgstr "Extensiounen"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Bildschiermfotoen"

#: include/footer.php:61
msgid "Community"
msgstr "Community"

#: include/footer.php:64
msgid "Forums"
msgstr "Foren"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Mail-Lëschten"

#: include/footer.php:66
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Suen spenden"

#: include/footer.php:68
msgid "Donate time"
msgstr "Zäit spenden"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Projet an Organisatioun"

#: include/footer.php:76
msgid "Team"
msgstr "Equipe"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Spigel-Serveren"

#: include/footer.php:83
msgid "Security center"
msgstr "Sécherheetszentrum"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Matmaachen"

#: include/footer.php:85
msgid "News"
msgstr "Neiegkeeten"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "VLC eroflueden"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Aner Systemer"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "Downloads bis elo"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"De VLC ass e fräien an Quell-oppenen Multimedia Player an en Bezuch-System "
"fir verschidden Betribssystemer, dat déi meeschten Multimedia-Dateien, wei "
"och DVDen, Audio-CDen, VCDen an verschidden Streaming-Protokoller ofspille "
"kann."

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"De VLC ass e fräien an quelloffenen Plattformiwwergräifenen Multimedia-"
"Player an Softwareinterface, deen déi meeschten Multimedia Dateien ofspillen "
"kann an mat vill verschidden Streaming Protokoller eens gëtt."

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC: Offiziell Säit – Fräi Multimedia Léisungen fir all Betribssystemer! "

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Aner Projeten vun VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Fir jiddereen"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"De VLC ass e leeschtungsstaarken Media Player den déi meeschten Media-"
"Codecen a Video-Formaten ofspille kann."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"De VideoLAN Movie Creator ass eng net-linear Bearbeschtungs-Software fir "
"d’Video-Erstellung. "

#: index.php:62
msgid "For Professionals"
msgstr "fir professionell Benotzung"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"Den DVBlast ass eng einfach a leeschtungsstaark MPEG-2/TS Demux- a Streaming-"
"Applicatioun."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"multicat ass en Tool-Set  fir einfach an effektiv Multicast-Streamen an "
"Transport-Streamen ze beaarbechten."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 ass eng fräi Applicatioun fir Video-Streamen an d’ H.264/MPEG-4 AVC "
"Format ze codéieren."

#: index.php:104
msgid "For Developers"
msgstr "Fir Entwéckler"

#: index.php:140
msgid "View All Projects"
msgstr "All Projeten ukucken"

#: index.php:144
msgid "Help us out!"
msgstr "Hëlleft eis!"

#: index.php:148
msgid "donate"
msgstr "spenden"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN ass eng gemengnëtzech Organisatioun."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"All eis Käschten ginn duerch Donen gedeckt, déi mir vun onseren Notzer "
"kréien. Wann Iech ee VideoLAN-Produit gefält, dann spent w.e.g., vir eis ze "
"ënnerstëtzen."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Gitt méi gewuer"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN ass Quell-oppen Software."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Dat bedeit, dass wann dir d’Fähegkeet an de Wonsch hutt, een vun onsere "
"Produiten ze verbesseren, är Beiträg ëmmer wëllkomm sinn."

#: index.php:187
msgid "Spread the Word"
msgstr "Sot et weider."

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Mir sinn der Meenung, dass VideoLAN déi beschte Software fir de beschte "
"Präis ubitt: gratis. Wann Dir dem zoustëmmt, dann hëlleft eis dobäi, eis "
"Software bekannt ze maachen."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Neiegkeeten &amp; Aktualiséierungen"

#: index.php:218
msgid "More News"
msgstr "Mei Neiegkeeten"

#: index.php:222
msgid "Development Blogs"
msgstr "Entwéckler-Bloggen"

#: index.php:251
msgid "Social media"
msgstr "Sozial Netzwierker"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Lued de VLC fir:"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Einfach, séier a leeschtungsstaark"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Spillt alles of"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Dateien, Disken, Webcams Peripheriegeräter an Streamen."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""
"Spillt déi meeschten Codecen of, ouni dass zousätzlech Codecpäck "
"erfuerderlech sinn."

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Leeft op alle Plattformen"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Komplett gratis"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "Keng Spyware, keng Werbebanner an keng Userverfolgung."

#: vlc/index.php:47
msgid "learn more"
msgstr "gitt méi gewuer"

#: vlc/index.php:66
msgid "Add"
msgstr "bäifügen"

#: vlc/index.php:66
msgid "skins"
msgstr "Themen"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Erstellt Themen mat"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "dem VLC skin-editor"

#: vlc/index.php:72
msgid "Install"
msgstr "Installéieren"

#: vlc/index.php:72
msgid "extensions"
msgstr "Extensiounen"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Kuckt all Screenshoten."

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Offiziell Downloaden vum VLC media Player."

#: vlc/index.php:146
msgid "Sources"
msgstr "Quellen"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Dir kennt och direkt den"

#: vlc/index.php:148
msgid "source code"
msgstr "Quell-Code eroflueden"
