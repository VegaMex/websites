# Asturian translation
# Copyright (C) 2020 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Iñigo Varela <ivarela@softastur.org>, 2014
# enolp <enolp@softastur.org>, 2019-2020
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2020-03-04 21:43+0100\n"
"Last-Translator: enolp <enolp@softastur.org>, 2020\n"
"Language-Team: Asturian (http://www.transifex.com/yaron/vlc-trans/language/"
"ast/)\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr "un proyeutu y una"

#: include/header.php:289
msgid "non-profit organization"
msgstr "organización ensin anímu de llucru"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "Socios"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr "Equipu y organización"

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr "Servicios de consultes y socios"

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "Eventos"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "Llegal"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "Centru de prensa"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "Contautu"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "Descarga"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "Carauterístiques"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr "Personalización"

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Consiguir caxigalines"

#: include/menus.php:51
msgid "Projects"
msgstr "Proyeutos"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "Tolos proyeutos"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "Collaboración"

#: include/menus.php:77
msgid "Getting started"
msgstr "Entamar"

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr "Donación"

#: include/menus.php:79
msgid "Report a bug"
msgstr "Informar d'un fallu"

#: include/menus.php:83
msgid "Support"
msgstr "Sofitu"

#: include/footer.php:33
msgid "Skins"
msgstr "Aspeutos"

#: include/footer.php:34
msgid "Extensions"
msgstr "Estensiones"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "Captures de pantalla"

#: include/footer.php:61
msgid "Community"
msgstr "Comunidá"

#: include/footer.php:64
msgid "Forums"
msgstr "Foru"

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "Llistes de corréu"

#: include/footer.php:66
msgid "FAQ"
msgstr "FAQ"

#: include/footer.php:67
msgid "Donate money"
msgstr "Dona dineru"

#: include/footer.php:68
msgid "Donate time"
msgstr "Dona tiempu"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "Proyeutu y organización"

#: include/footer.php:76
msgid "Team"
msgstr "Equipu"

#: include/footer.php:80
msgid "Mirrors"
msgstr "Espeyos"

#: include/footer.php:83
msgid "Security center"
msgstr "Centru de seguranza"

#: include/footer.php:84
msgid "Get Involved"
msgstr "Andechar"

#: include/footer.php:85
msgid "News"
msgstr "Noticies"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "Baxar VLC"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "Otros sistemes"

#: include/os-specific.php:260
msgid "downloads so far"
msgstr "descargues hasta'l momentu"

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""
"VLC ye un reproductor y framework gratuitu, de códigu abiertu y "
"multiplataforma que reproduz la mayoría de ficheros multimedia amás de DVDs, "
"CDs d'audiu, VCDs y varios protocolos de tresmisión"

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""
"VLC ye un reproductor y framework gratuitu, de códigu abiertu y "
"multiplataforma que reproduz la mayoría de ficheros multimedia y varios "
"protocolos de tresmisión"

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr ""
"VLC: El sitiu oficial - ¡Soluciones multimedia llibres y gratuites pa tolos "
"SO!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "Otros proyeutos de VideoLAN"

#: index.php:30
msgid "For Everyone"
msgstr "Pa tol mundu"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC ye un reproductor multimedia poderosu que reproduz la mayoría de códecs "
"y formatos d'audiu y videu disponibles."

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN Movie Creator ye un programa d'edición non llinial pa la creación "
"de vídeos."

#: index.php:62
msgid "For Professionals"
msgstr "Pa profesionales"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast ye un demultiplexador cenciellu y poderosu pa MPEG-2/TS y "
"aplicaciones de tresmisión."

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
"Multicat ye un conxuntu de ferramientes diseñaes pa manipular fluxos "
"multiespardimientu y TS de mou fácil y eficiente."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x264 ye un aplicación llibre y gratuita pa codificar fluxos de videu nel "
"formatu H.264/MPEG-4 AVC "

#: index.php:104
msgid "For Developers"
msgstr "Pa desendolcadores"

#: index.php:140
msgid "View All Projects"
msgstr "Ver tolos proyeutos"

#: index.php:144
msgid "Help us out!"
msgstr "¡Échanos un gabitu!"

#: index.php:148
msgid "donate"
msgstr "Donar"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN ye una organización ensin ánimu de llucru."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"Tolos nuetros gastos cúbrense coles donaciones que recibimos de los "
"usuarios. Si esfrutes col usu de los productos de VideoLAN, dona pa "
"sofitanos."

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "Deprender más"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN ye software de códigu llibre."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"Esto significa que si tienes l'habilidá y el deséu d'ameyorar dalgún de los "
"nuesos productos, agradecemos l'ayuda"

#: index.php:187
msgid "Spread the Word"
msgstr "Espardimientu"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"Cuidamos que VideoLAN tien el meyor software de videu disponible al meyor "
"preciu. Si tas d'acuerdu, espardi ente la xente los nuesos programes."

#: index.php:215
msgid "News &amp; Updates"
msgstr "Noticies y anovamientos"

#: index.php:218
msgid "More News"
msgstr "Más noticies"

#: index.php:222
msgid "Development Blogs"
msgstr "Blogues del desendolcu"

#: index.php:251
msgid "Social media"
msgstr "Redes sociales"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""
"La descarga oficial del reproductor multimedia VLC, el meyor reproductor de "
"códigu abiertu"

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "Consiguir VLC pa"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr "Cenciellez, rapidez y poderíu"

#: vlc/index.php:35
msgid "Plays everything"
msgstr "Reproduz de too"

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr "Ficheros, discos, cámares web, preseos y fluxos."

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr "Reproduz la mayoría de códecs ensin nada más"

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr "Execútase en toles plataformes"

#: vlc/index.php:44
msgid "Completely Free"
msgstr "Llibre y gratuitu dafechu"

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr "Ensin spyware, anuncios nin rastrexu d'usuarios."

#: vlc/index.php:47
msgid "learn more"
msgstr "deprender más"

#: vlc/index.php:66
msgid "Add"
msgstr "Amiesta"

#: vlc/index.php:66
msgid "skins"
msgstr "aspeutos"

#: vlc/index.php:69
msgid "Create skins with"
msgstr "Crea aspeutos con"

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr "Editor d'aspeutos pa VLC"

#: vlc/index.php:72
msgid "Install"
msgstr "Instala"

#: vlc/index.php:72
msgid "extensions"
msgstr "estensiones"

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "Ver toles captures de pantalla"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "Descargues oficiales del reproductor multimedia VLC"

#: vlc/index.php:146
msgid "Sources"
msgstr "Códigu"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "Tamién pues consiguir direutamente'l"

#: vlc/index.php:148
msgid "source code"
msgstr "códigu fonte"

#~ msgid "A project and a"
#~ msgstr "Un proyeutu y una"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "ensamada por voluntarios, que desendolquen y promocionen soluciones de "
#~ "software multimedia llibre y de códigu abiertu."

#~ msgid "why?"
#~ msgstr "por qué?"

#~ msgid "Home"
#~ msgstr "Aniciu"

#~ msgid "Support center"
#~ msgstr "Centru de soporte"

#~ msgid "Dev' Zone"
#~ msgstr "Dev' Zone"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "Un reproductor de medios simple, rápidu y perpoderosu."

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Reproduzlo too: Ficheros, Discos, Webcams, Streams y preseos."

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr ""
#~ "Reproduz la mayoría de los códecs ensin necesidá de paquetes adicionales:"

#~ msgid "Runs on all platforms:"
#~ msgstr "Executable en toles plataformes:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr "Llibre dafechu, 0 spyware, 0 ads y ensin siguimientu al usuariu"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "Pue convertir y facer streaming"

#~ msgid "Discover all features"
#~ msgstr "Descubrir toles carauterístiques"

#~ msgid "DONATE"
#~ msgstr "DONA"

#~ msgid "Other Systems and Versions"
#~ msgstr "Otros sistemes y versiones"

#~ msgid "Other OS"
#~ msgstr "Otros Sistemes Operativos"
