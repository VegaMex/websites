# Bengali (India) translation
# Copyright (C) 2020 VideoLAN
# This file is distributed under the same license as the vlc package.
#
# Translators:
# Anomitra Saha <anomitra1993@gmail.com>, 2015
# BIRAJ KARMAKAR <brnet00@gmail.com>, 2012
# Umesh Agarwal <umesh.agarwal1@gmail.com>, 2012
msgid ""
msgstr ""
"Project-Id-Version: VideoLAN's websites\n"
"Report-Msgid-Bugs-To: vlc-devel@videolan.org\n"
"POT-Creation-Date: 2020-02-05 18:13+0100\n"
"PO-Revision-Date: 2017-10-06 14:28+0200\n"
"Last-Translator: VideoLAN <videolan@videolan.org>, 2017\n"
"Language-Team: Bengali (India) (http://www.transifex.com/yaron/vlc-trans/"
"language/bn_IN/)\n"
"Language: bn_IN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: include/header.php:289
msgid "a project and a"
msgstr ""

#: include/header.php:289
msgid "non-profit organization"
msgstr "অলাভজনক সংস্থা"

#: include/header.php:298 include/footer.php:79
msgid "Partners"
msgstr "সঙ্গী"

#: include/menus.php:32
msgid "Team &amp; Organization"
msgstr ""

#: include/menus.php:33
msgid "Consulting Services &amp; Partners"
msgstr ""

#: include/menus.php:34 include/footer.php:82
msgid "Events"
msgstr "ঘটনাবলী"

#: include/menus.php:35 include/footer.php:77 include/footer.php:111
msgid "Legal"
msgstr "আইনগত"

#: include/menus.php:36 include/footer.php:81
msgid "Press center"
msgstr "প্রেস মধ্য"

#: include/menus.php:37 include/footer.php:78
msgid "Contact us"
msgstr "যোগাযোগ আমাদেরকে"

#: include/menus.php:43 include/os-specific.php:279
msgid "Download"
msgstr "ডাউনলোড করুন"

#: include/menus.php:44 include/footer.php:35
msgid "Features"
msgstr "বৈশিষ্ট্য"

#: include/menus.php:45 vlc/index.php:57 vlc/index.php:63
msgid "Customize"
msgstr ""

#: include/menus.php:47 include/footer.php:69
msgid "Get Goodies"
msgstr "Goodies পান"

#: include/menus.php:51
msgid "Projects"
msgstr "প্রকল্প"

#: include/menus.php:71 include/footer.php:41
msgid "All Projects"
msgstr "সমস্ত প্রকল্প"

#: include/menus.php:75 index.php:168
msgid "Contribute"
msgstr "কাজে লাগা"

#: include/menus.php:77
msgid "Getting started"
msgstr ""

#: include/menus.php:78 include/menus.php:96
msgid "Donate"
msgstr ""

#: include/menus.php:79
msgid "Report a bug"
msgstr "একটি বাগ রিপোর্ট করুন"

#: include/menus.php:83
msgid "Support"
msgstr "সমর্থন"

#: include/footer.php:33
msgid "Skins"
msgstr "স্কিনস"

#: include/footer.php:34
msgid "Extensions"
msgstr "এক্সটেনশানগুলি"

#: include/footer.php:36 vlc/index.php:83
msgid "Screenshots"
msgstr "স্ক্রীনশট"

#: include/footer.php:61
msgid "Community"
msgstr "সম্প্রদায়"

#: include/footer.php:64
msgid "Forums"
msgstr "আড্ডা "

#: include/footer.php:65
msgid "Mailing-Lists"
msgstr "মেলিং তালিকা"

#: include/footer.php:66
msgid "FAQ"
msgstr "সম্ভাব্য প্রশ্নগুলি"

#: include/footer.php:67
msgid "Donate money"
msgstr "টাকা দান করা "

#: include/footer.php:68
msgid "Donate time"
msgstr " সময় দান করা"

#: include/footer.php:75
msgid "Project and Organization"
msgstr "প্রকল্প এবং সংস্থা"

#: include/footer.php:76
msgid "Team"
msgstr "দল"

#: include/footer.php:80
msgid "Mirrors"
msgstr ""

#: include/footer.php:83
msgid "Security center"
msgstr "নিরাপত্তা কেন্দ্র "

#: include/footer.php:84
msgid "Get Involved"
msgstr "জড়িত হন"

#: include/footer.php:85
msgid "News"
msgstr "সংবাদ"

#: include/os-specific.php:103
msgid "Download VLC"
msgstr "VLC ডাউনলোড করুন"

#: include/os-specific.php:109 include/os-specific.php:295 vlc/index.php:168
msgid "Other Systems"
msgstr "অন্যান্য প্লাটফর্মে "

#: include/os-specific.php:260
msgid "downloads so far"
msgstr ""

#: include/os-specific.php:648
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and "
"various streaming protocols."
msgstr ""

#: include/os-specific.php:652
msgid ""
"VLC is a free and open source cross-platform multimedia player and framework "
"that plays most multimedia files, and various streaming protocols."
msgstr ""

#: index.php:4
msgid "VLC: Official site - Free multimedia solutions for all OS!"
msgstr "VLC: অফিসিয়াল সাইট - ফ্রি সকল OS এর জন্য মাল্টিমিডিয়া সমাধান!"

#: index.php:26
msgid "Other projects from VideoLAN"
msgstr "VideoLAN থেকে অন্যান্য প্রকল্প"

#: index.php:30
msgid "For Everyone"
msgstr "প্রত্যের জন্য"

#: index.php:40
msgid ""
"VLC is a powerful media player playing most of the media codecs and video "
"formats out there."
msgstr ""
"VLC মিডিয়া কোডেক এবং ভিডিও বিন্যাস বাইরে সেখানের একটি ক্ষমতাশালী মিডিয়া চালক "
"চালানো হচ্ছে সর্বাপেক্ষা।"

#: index.php:53
msgid ""
"VideoLAN Movie Creator is a non-linear editing software for video creation."
msgstr ""
"VideoLAN চলচ্চিত্র সৃষ্টিকারী ভিডিও সৃষ্টির জন্য একটি non-linear সম্পাদন সফটওয়্যার।"

#: index.php:62
msgid "For Professionals"
msgstr "পেশাদারদের জন্য"

#: index.php:72
msgid ""
"DVBlast is a simple and powerful MPEG-2/TS demux and streaming application."
msgstr ""
"DVBlast একটি সহজ এবং ক্ষমতাশালী MPEG-2/TS ডিমিউক্স এবং স্ট্রীমিং অ্যাপলিকেশন।"

#: index.php:82
msgid ""
"multicat is a set of tools designed to easily and efficiently manipulate "
"multicast streams and TS."
msgstr ""
" সহজভাবেতে সরঞ্জামের একটি সেট পরিকল্পনা করে এবং মাল্টিকাস্ট স্ট্রীম এবং TS "
"efficiently নিপুণভাবে ব্যবহার করে।ast streams and TS."

#: index.php:95
msgid ""
"x264 is a free application for encoding video streams into the H.264/MPEG-4 "
"AVC format."
msgstr ""
"x২৬৪ H 264/MPEG-4 AVC ফরম্যাটের দিকে ভিডিও স্ট্রীম এনকোডিং করার জন্য একটি মুক্ত "
"অ্যাপলিকেশন।"

#: index.php:104
msgid "For Developers"
msgstr "ডেভেলপারগণের জন্য"

#: index.php:140
msgid "View All Projects"
msgstr "দেখা সমস্ত প্রকল্প"

#: index.php:144
msgid "Help us out!"
msgstr "আমাদের কে সহায়তা করুন!"

#: index.php:148
msgid "donate"
msgstr "দান করা"

#: index.php:156
msgid "VideoLAN is a non-profit organization."
msgstr "VideoLAN একটি অলাভজনক সংস্থা."

#: index.php:157
msgid ""
" All our costs are met by donations we receive from our users. If you enjoy "
"using a VideoLAN product, please donate to support us."
msgstr ""
"সমস্ত আমাদের খরচ আমাদের ব্যবহারকারী থেকে donations আমাদের মধ্যে দেখা করা হয় "
"গ্রহন করা হয়। যদি আপনি একটি VideoLAN দ্রব্য ব্যবহার করে উপভোগ করেন, দয়া করে "
"আমাদেরকে সমর্থন করতে দান করুন"

#: index.php:160 index.php:180 index.php:198
msgid "Learn More"
msgstr "আরো শিখুন"

#: index.php:176
msgid "VideoLAN is open-source software."
msgstr "VideoLAN হয় ওপেন সোর্স সফ্টওয়্যার."

#: index.php:177
msgid ""
"This means that if you have the skill and the desire to improve one of our "
"products, your contributions are welcome"
msgstr ""
"এইটি অভিপ্রায় কর সেটায় যদি আপনার দক্ষতা রয়েছে এবং আমাদের দ্রব্যের একটি উন্নতি "
"করতে আকাঙ্খা, আপনার অবদানসমূহ স্বাগত আছে"

#: index.php:187
msgid "Spread the Word"
msgstr "শব্দ ছড়িয়ে দিন"

#: index.php:195
msgid ""
"We feel that VideoLAN has the best video software available at the best "
"price: free. If you agree please help spread the word about our software."
msgstr ""
"আমরা অনুভব করি যে VideoLANএর সর্বোত্তম দামে সর্বোত্তম ভিডিও সফটওয়্যার প্রাপ্তিসাধ্য "
"রয়েছে: মুক্ত। যদি আপনি আমাদের সফটওয়্যারের সম্বন্ধে দয়া করে কথা সম্মত হন সাহায্য "
"করেন প্রসারিত হন।"

#: index.php:215
msgid "News &amp; Updates"
msgstr "সংবাদ আপডেট"

#: index.php:218
msgid "More News"
msgstr ""

#: index.php:222
msgid "Development Blogs"
msgstr "ডেভেলপমেন্ট ব্লগ"

#: index.php:251
msgid "Social media"
msgstr "সামাজিক মিডিয়া"

#: vlc/index.php:3
msgid "Official download of VLC media player, the best Open Source player"
msgstr ""

#: vlc/index.php:21
msgid "Get VLC for"
msgstr "জন্য VLC পান"

#: vlc/index.php:29 vlc/index.php:32
msgid "Simple, fast and powerful"
msgstr ""

#: vlc/index.php:35
msgid "Plays everything"
msgstr ""

#: vlc/index.php:35
msgid "Files, Discs, Webcams, Devices and Streams."
msgstr ""

#: vlc/index.php:38
msgid "Plays most codecs with no codec packs needed"
msgstr ""

#: vlc/index.php:41
msgid "Runs on all platforms"
msgstr ""

#: vlc/index.php:44
msgid "Completely Free"
msgstr ""

#: vlc/index.php:44
msgid "no spyware, no ads and no user tracking."
msgstr ""

#: vlc/index.php:47
msgid "learn more"
msgstr ""

#: vlc/index.php:66
msgid "Add"
msgstr ""

#: vlc/index.php:66
msgid "skins"
msgstr ""

#: vlc/index.php:69
msgid "Create skins with"
msgstr ""

#: vlc/index.php:69
msgid "VLC skin editor"
msgstr ""

#: vlc/index.php:72
msgid "Install"
msgstr ""

#: vlc/index.php:72
msgid "extensions"
msgstr ""

#: vlc/index.php:126
msgid "View all screenshots"
msgstr "দেখা সমস্ত স্ক্রীনশট"

#: vlc/index.php:135
msgid "Official Downloads of VLC media player"
msgstr "VLC মিডিয়া চালকের সরকারী ডাউনলোড"

#: vlc/index.php:146
msgid "Sources"
msgstr "উৎস"

#: vlc/index.php:147
msgid "You can also directly get the"
msgstr "আপনি ও সরাসরিভাবে পেতে পারেন"

#: vlc/index.php:148
msgid "source code"
msgstr "উৎ‍স কোড"

#~ msgid "A project and a"
#~ msgstr "একটি প্রকল্প এবং একটি"

#~ msgid ""
#~ "composed of volunteers, developing and promoting free, open-source "
#~ "multimedia solutions."
#~ msgstr ""
#~ "স্বেচ্ছাসেবকদের গঠিত, উন্নয়নশীল এবং বিনামূল্যে, ওপেন সোর্স মাল্টিমিডিয়া সমাধান "
#~ "প্রচার."

#~ msgid "why?"
#~ msgstr "কেন?"

#~ msgid "Home"
#~ msgstr "ঘর"

#~ msgid "Support center"
#~ msgstr "সমর্থন মধ্য"

#~ msgid "Dev' Zone"
#~ msgstr "দেব 'অঞ্চল"

#~ msgid "Simple, fast and powerful media player."
#~ msgstr "সহজ, এবং দ্রুত ক্ষমতাশালী মিডিয়া চালক।"

#~ msgid "Plays everything: Files, Discs, Webcams, Devices and Streams."
#~ msgstr "Plays সবকিছু: ফাইল, Discs, Webcams, ডিভাইস এবং স্ট্রীম।"

#~ msgid "Plays most codecs with no codec packs needed:"
#~ msgstr "কোডেক গোছানো ব্যতীত Plays কোডেক সর্বাপেক্ষা প্রয়োজন বোধ করেছিল:"

#~ msgid "Runs on all platforms:"
#~ msgstr "সমস্ত প্ল্যাটফর্মে চল:"

#~ msgid "Completely Free, no spyware, no ads and no user tracking."
#~ msgstr "মুক্ত, সম্পূর্ণভাবে স্পাইওয়্যার, ads এবং কোনও ব্যবহারকারী tracking না।"

#~ msgid "Can do media conversion and streaming."
#~ msgstr "মিডিয়া স্ট্রিমিং এবং রূপান্তর করতে পারি."

#~ msgid "Discover all features"
#~ msgstr "Discover সমস্ত বৈশিষ্ট্য"

#~ msgid "DONATE"
#~ msgstr "দান করা"

#~ msgid "Other Systems and Versions"
#~ msgstr "অন্যান্য সিস্টেম এবং সংস্করণ"

#~ msgid "Other OS"
#~ msgstr "অন্যান্য অপারেটিং সিস্টেম"
