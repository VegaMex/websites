<?php
   $title = "VLC media player for Debian GNU/Linux";
   $new_design = true;
   $lang = "en";
   $menu = array( "vlc", "download" );

   $additional_js = array("/js/slimbox2.js", "/js/slick-init.js", "/js/slick.min.js");
   $additional_css = array("/js/css/slimbox2.css", "/style/slick.min.css", "/style/panels.css");
   require($_SERVER["DOCUMENT_ROOT"]."/include/header.php");
   include($_SERVER["DOCUMENT_ROOT"]."/include/os-specific.php");
   include($_SERVER["DOCUMENT_ROOT"]."/include/package.php");
?>

<div class="container">
	<?php
	$screenshots = getScreenshots("linux");
	$defaultDetail = getOS("linux");
	?>
    <section class="download-wrapper">
        <div class="row reorder-xs">
            <?php drawScreenshots($screenshots); ?>
            <div class="v-align col-sm-5">
                <div class="center-font-xs">
                    <?php image('largeVLC.png', 'Large Orange VLC media player Traffic Cone Logo', 'big-vlc-img img-responsive visible-xs-inline-block v-align'); ?>
                    <h1 class="v-align bigtitle">
                        VLC media player for <a href="http://debian.org">Debian GNU/Linux</a>
                    </h1>
                </div>
                <div class="projectDescription hidden-sm hidden-xs">
                    <?php echo
                    _("VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."); ?>
                </div>
                <div class="projectDescription visible-xs visible-sm center-font-xs">
                    <?php echo
                    _("VLC is a free and open source cross-platform multimedia player and framework that plays most multimedia files as well as DVDs, Audio CDs, VCDs, and various streaming protocols."); ?>
                </div>
            </div>
        </div>
    </section>

<div id="left">

<h2> Debian stable (buster) and testing (bullseye)</h2>
<p> Debian buster and bullseye follow the 3.0.x release branch of <a href="/vlc/">VLC</a>.</p>

<h2> Debian oldstable (stretch)</h2>
<p> Debian stretch follows the 3.0.x release branch of <a href="/vlc/">VLC</a></p>

<h2> Debian oldoldstable (jessie)</h>
<p> Debian jessie follows the 2.2.x release branch of <a href="/vlc/">VLC</a>.</p>

<h2> Debian unstable (sid) </h2>
<p> VLC's latest packaged version is always in the official Debian unstable branch.</p>

<h2>Installation the Graphical way</h2>
<p>If there is an GUI front-end for APT on your system You can install via <a href="apt://vlc">APT link</a></p>

<h2>Installation the Command line way</h2>
<p><blockquote>
<pre>
% sudo apt install vlc
</pre>
</blockquote>
</p>

<h3>Nota Bene</h3>
<p class="projectDescription">VLC for Debian and many other Linux distributions is also packaged using <a href="https://snapcraft.io/vlc/">snap packages</a>.
This allows us to distribute latest and greatest VLC versions directly to end
users, with security and critical bug fixes, full codec and optical media
support.  <br />
To install via snap:
<p><blockquote>
<pre>
% sudo snap install vlc
</pre>
</blockquote>
</p>

<?php panel_start( "blue" ); ?>
<h1>Playing DVD</h1>

To play DVD, you need to install the libdvdcss package.</p>

<?php panel_end(); ?>

</div>
</div>

<?php
  footer('$Id$');
?>
